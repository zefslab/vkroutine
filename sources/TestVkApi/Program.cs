﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Enums.SafetyEnums;
using VkNet.Model.RequestParams;
using static System.Console;

namespace TestVkApi
{
    class Program
    {
        static void Main(string[] args)
        {
            ulong appID = 100000;                      // ID приложения
            string email = "zefslab@gmail.com";         // email или телефон
            string pass = "xxx";               // пароль для авторизации
            Settings scope = Settings.Friends;      // Приложение имеет доступ к друзьям

            var vk = new VkApi();
            vk.Authorize(new ApiAuthParams
            {
                ApplicationId = appID,
                Login = email,
                Password = pass,
                Settings = scope
            });

            var groups = vk.Groups.Search(new GroupsSearchParams()
            {
                Query = "javarush"
            });

            foreach (var g in groups)
            {
                if (g.Id == 43948962)
                {
                    var members = vk.Groups.GetMembers(new GroupsGetMembersParams
                    {
                        GroupId = g.Id.ToString(),
                    });

                    foreach (var u in members)
                    {
                        var user = vk.Users.Get(u.Id, ProfileFields.All);
                        if (user.City != null)
                        {
                            WriteLine($"{user.LastName}  {user.FirstName} из {user.City.Title}");

                            if (user.City.Title == "Кемерово")
                            {
                                WriteLine("-------------------------------------------------------");
                                WriteLine($"{user.LastName}  {user.FirstName} из {user.City.Title}");
                                WriteLine("-------------------------------------------------------");
                            }
                        }
                    }
                }

            }
            

        }

       
}
}
